import React from 'react'

const certificate = () => {
  return (
    <>
    <div style={{height:'100vh', width:'100vw',overflow:'hidden'}} className="">
        <iframe width="100%" height='100%' src="assets/images/portfolio/certificate.pdf" frameborder="0"></iframe>
    </div>
    </>
  )
}

export default certificate