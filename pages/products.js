import Link from "next/link";
import { useState } from "react";
import { Accordion } from "react-bootstrap";
import Slider from "react-slick";
import OrgariumAcc from "../src/components/OrgariumAcc";
import PageBanner from "../src/components/PageBanner";
import Layout from "../src/layouts/Layout";
import { heroSliderOne, serviceSliderOne } from "../src/sliderProps";
const Shop = () => {
  const [active, setActive] = useState("collapse0");

  return (
    <Layout>
      {/* <PageBanner pageTitle={"Shop"} pageName="Shop" /> */}
      <section className="hero-area-three">

        {/* Hero Section or Banner Section */}
        <Slider {...heroSliderOne} className="hero-slider-one">
          <div className="single-slider p-r z-1">
            <div
              className="image-layer bg_cover"
              style={{
                backgroundImage:
                  "url(assets/images/hero/h1.png)",
              }}
            />
            <div className="brand-card dark-black-bg">
              <img style={{ height: '100px' }} src="assets/images/logo/logo-1.png" alt="" />
              {/* <h3>Orgarium</h3> */}
            </div>
            <div className="container">
              <div className="row">
                <div className="col-lg-6">
                  <div className="hero-content text-white">
                    <span
                      className="tag-line"
                      data-animation="fadeInDown"
                      data-delay=".4s"
                    >
                      Organic Farms
                    </span>
                    <h1 data-animation="fadeInUp" data-delay=".5s">
                      Agriculture Farming
                    </h1>
                    <div
                      className="hero-button"
                      data-animation="fadeInDown"
                      data-delay=".6s"
                    >
                      <Link href="/about">
                        <a className="main-btn btn-yellow">Learn About Us</a>
                      </Link>
                      {/* <Link href="/portfolio-grid">
                        <a className="main-btn btn-white">Latest Project</a>
                      </Link> */}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="single-slider p-r z-1">
            <div
              className="image-layer bg_cover"
              style={{
                backgroundImage:
                  "url(assets/images/hero/h2.png)",
              }}
            />
            <div className="brand-card dark-black-bg">
              {/* <img src="assets/images/icon/wheat.png" alt="" /> */}
              <img style={{ height: '100px' }} src="assets/images/logo/logo-1.png" alt="" />

              {/* <h3>Orgarium</h3> */}
            </div>
            <div className="container">
              <div className="row">
                <div className="col-lg-6">
                  <div className="hero-content text-white">
                    <span
                      className="tag-line"
                      data-animation="fadeInDown"
                      data-delay=".4s"
                    >
                      Organic Farms
                    </span>
                    <h1 data-animation="fadeInUp" data-delay=".5s">
                      Agriculture Farming
                    </h1>
                    <div
                      className="hero-button"
                      data-animation="fadeInDown"
                      data-delay=".6s"
                    >
                      <Link href="/about">
                        <a className="main-btn btn-yellow">Learn About Us</a>
                      </Link>
                      {/* <Link href="/portfolio-grid">
                        <a className="main-btn btn-white">Latest Project</a>
                      </Link> */}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="single-slider p-r z-1">
            <div
              className="image-layer bg_cover"
              style={{
                backgroundImage:
                  "url(assets/images/hero/h3.png)",
              }}
            />
            <div className="brand-card dark-black-bg">
              {/* <img src="assets/images/icon/wheat.png" alt="" /> */}
              <img style={{ height: '100px' }} src="assets/images/logo/logo-1.png" alt="" />
              {/* <h3>Orgarium</h3> */}
            </div>
            <div className="container">
              <div className="row">
                <div className="col-lg-6">
                  <div className="hero-content text-white">
                    <span
                      className="tag-line"
                      data-animation="fadeInDown"
                      data-delay=".4s"
                    >
                      Organic Farms
                    </span>
                    <h1 data-animation="fadeInUp" data-delay=".5s">
                      Agriculture Farming
                    </h1>
                    <div
                      className="hero-button"
                      data-animation="fadeInDown"
                      data-delay=".6s"
                    >
                      <Link href="/about">
                        <a className="main-btn btn-yellow">Learn About Us</a>
                      </Link>
                      {/* <Link href="/portfolio-full-width">
                        <a className="main-btn btn-white">Latest Project</a>
                      </Link> */}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </Slider>
        {/* Hero Section or Banner Section ends */}

        {/*====== Start Features Section ======*/}
        <section className="features-section border-bottom-1">
          <div className="features-wrap-two wow fadeInUp">
            <div className="features-item-two">
              <div className="fill-number">01</div>
              <div className="icon">
                <i className="flaticon-tractor" />
              </div>
              <div className="text">
                <h5>Modern Agriculture Equipments</h5>
              </div>
            </div>
            <div className="features-item-two">
              <div className="fill-number">02</div>
              <div className="icon">
                <i className="flaticon-agriculture" />
              </div>
              <div className="text">
                <h5>Organic Products and Plantation</h5>

              </div>
            </div>
            <div className="features-item-two">
              <div className="fill-number">03</div>
              <div className="icon">
                <i className="flaticon-social-care" />
              </div>
              <div className="text">
                <h5>Professional and Expert Technical & Operation Team</h5>
              </div>
            </div>
            {/* <div className="features-item-two">
              <div className="fill-number">04</div>
              <div className="icon">
                <i className="flaticon-farming" />
              </div>
              <div className="text">
                <h5>Provide Full of Natural Goodness</h5>
              </div>
            </div> */}
          </div>
        </section>
        {/*====== End Features Section ======*/}

        {/*====== Our Products Section ======*/}
        <section className="service-two dark-black-bg pt-165">
          <div className="container">
            <div className="row justify-content-center">
              <div className="col-xl-6 col-lg-10">
                <div className="section-title section-title-white text-center mb-50 wow fadeInDown">
                  <span className="sub-title">Our Products</span>
                  <h2>What We Provide For Your Better Health</h2>
                </div>
              </div>
            </div>
            <Slider {...serviceSliderOne} className="service-slider-one">
              <div className="single-service-item-two text-center wow fadeInUp">
                <div className="img-holder">
                  <img src="assets/images/portfolio/product-5.png" alt="" />
                </div>
                <div className="text">
                  <h3 className="title">
                    <Link href="/service-details">
                      <a>Green Fresh Leaves</a>
                    </Link>
                  </h3>
                  <p style={{ textAlign: 'justify' }}>
                    Our leaves is produced from an organic certified plantations in Sg Tengi ...

                  </p>
                  <Link href="service-details/#greenFreshLeaves">
                    <a className="main-btn btn-yellow">Read More</a>
                  </Link>
                </div>
              </div>
              <div className="single-service-item-two text-center wow fadeInDown">
                <div className="img-holder">
                  <img src="assets/images/portfolio/product-1.png" alt="" />
                </div>
                <div className="text">
                  <h3 className="title">
                    <Link href="/service-details">
                      <a>Lemon Myrtle Spices</a>
                    </Link>
                  </h3>
                  <p style={{ textAlign: 'justify' }}>
                    Our processing of the dry herb is second to none, done in such
                    a way to lock ...
                  </p>
                  <Link href="service-details/#lemonSpices">
                    <a className="main-btn btn-yellow">Read More</a>
                  </Link>
                </div>
              </div>
              <div className="single-service-item-two text-center wow fadeInUp">
                <div className="img-holder">
                  <img src="assets/images/portfolio/product-4.png" alt="" />
                </div>
                <div className="text">
                  <h3 className="title">
                    <Link href="/service-details">
                      <a>Lemon Myrtle Oil</a>
                    </Link>
                  </h3>
                  <p style={{ textAlign: 'justify' }}>
                    Our oil is the citral chemotype, arguably the richest source of this vital
                    ...

                  </p>
                  <Link href="service-details/#lemonMartleOil">
                    <a className="main-btn btn-yellow">Read More</a>
                  </Link>
                </div>
              </div>
              {/* <div className="single-service-item-two text-center wow fadeInDown">
                <div className="img-holder">
                  <img src="assets/images/service/img-3.jpg" alt="" />
                </div>
                <div className="text">
                  <h3 className="title">
                    <Link href="/service-details">
                      <a>Milks &amp; Meat</a>
                    </Link>
                  </h3>
                  <p>
                    Sit amet consectetur adipisc elit sed eiusmod tempor incdunt
                    labore et dolore magna
                  </p>
                  <Link href="/service-details">
                    <a className="main-btn btn-yellow">Read More</a>
                  </Link>
                </div>
              </div> */}
            </Slider>
          </div>
        </section>
        {/*====== Our Products Section ======*/}

        {/*====== Start Faq Section ======*/}
        {/* <section className="faqs-section pt-130 pb-80">
          <div className="container">
            <div className="row">
              <div className="col-lg-6">
                <div className="faq-one_img-box mb-50">
                  <img
                    src="assets/images/faq/faq-1.jpg"
                    className="faq-img-one wow fadeInUp"
                    alt="Faq Image"
                  />
                  <img
                    src="assets/images/faq/faq-2.jpg"
                    className="faq-img-two wow fadeInDown"
                    alt="Faq Image"
                  />
                </div>
              </div>
              <div className="col-lg-6">
                <div className="faq-one_content-box mb-50 pl-lg-70 wow fadeInRight">
                  <div className="section-title mb-60">
                    <span className="sub-title">Why Choose Us</span>
                    <h2>Why People’s Choose Farming Products</h2>
                  </div>
                  <Accordion
                    defaultActiveKey="collapse0"
                    className="accordion"
                    id="accordionOne"
                  >
                    <OrgariumAcc
                      title={"Best Organic Food Provider Since 1995"}
                      event={"collapse0"}
                      onClick={() => setActive("collapse0")}
                      active={active}
                    />
                    <OrgariumAcc
                      title={" Why Choose Our Products ?"}
                      event={"collapse1"}
                      onClick={() => setActive("collapse1")}
                      active={active}
                    />
                    <OrgariumAcc
                      title={"Organic Food and Biology Safe ?"}
                      event={"collapse2"}
                      onClick={() => setActive("collapse2")}
                      active={active}
                    />
                    <OrgariumAcc
                      title={"How Much Sweet &amp; Testy Our Foods ?"}
                      event={"collapse3"}
                      onClick={() => setActive("collapse3")}
                      active={active}
                    />
                  </Accordion>
                </div>
              </div>
            </div>
          </div>
        </section> */}
        {/*====== End Faq Section ======*/}

        {/*====== Start CTA Section ======*/}
        <section className="cta-one pt-100 pb-100">
          <div className="container-fluid">
            <div className="cta-wrap-one">
              <div className="row">
                <div className="col-xl-6 col-lg-12">
                  <div
                    className="cta-item_one bg_cover text-white mb-40 wow fadeInLeft"
                    style={{
                      backgroundImage: "url(assets/images/cta/cta-1.jpg)",
                    }}
                  >
                    <div className="text d-flex justify-content-between align-items-center">
                      <h2>Need Organic Food ?</h2>
                      <Link href="/contact">
                        <a className="main-btn bordered-btn bordered-white">
                          Get Quote
                        </a>
                      </Link>
                    </div>
                  </div>
                </div>
                <div className="col-xl-6 col-lg-12">
                  <div
                    className="cta-item_one bg_cover text-white mb-40 wow fadeInRight"
                    style={{
                      backgroundImage: "url(assets/images/cta/cta-2.jpg)",
                    }}
                  >
                    <div className="text d-flex justify-content-between align-items-center">
                      <h2>Have Any Equipments ?</h2>
                      <Link href="/contact">
                        <a className="main-btn bordered-btn bordered-white">
                          Contact Us
                        </a>
                      </Link>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        {/*====== End CTA Section ======*/}

      </section>
      {/* <section className="shaop-page pt-170 pb-70">
        <div className="container">
          <div className="product-search-filter wow fadeInUp">
            <form onSubmit={(e) => e.preventDefault()}>
              <div className="row align-items-center">
                <div className="col-lg-3">
                  <div className="product-search mb-30">
                    <div className="form_group">
                      <input
                        type="search"
                        className="form_control"
                        placeholder="Search"
                        name="search"
                      />
                      <button className="search-btn">
                        <i className="far fa-search" />
                      </button>
                    </div>
                  </div>
                </div>
                <div className="col-lg-9">
                  <div className="row justify-content-between align-items-center mb-15">
                    <div className="col-lg-4 col-md-6">
                      <div className="show-text mb-15">
                        <p>Showing 1 - 12 of 30 Results</p>
                      </div>
                    </div>
                    <div className="col-lg-8 col-md-6">
                      <div className="filter-category mb-15">
                        <ul>
                          <li>
                            <select className="wide">
                              <option data-display="Sort by Newness">
                                Sort by Newness
                              </option>
                              <option value={1}>Price High To Low</option>
                              <option value={2}>Price Low To High</option>
                            </select>
                          </li>
                          <li>
                            <Link href="/products">
                              <a>
                                <i className="far fa-list" />
                              </a>
                            </Link>
                          </li>
                          <li>
                            <Link href="/products">
                              <a>
                                <i className="far fa-th" />
                              </a>
                            </Link>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </form>
          </div>
          <div className="row">
            <div className="col-xl-12">
              <div className="products-wrapper">
                <div className="row">
                  <div className="col-xl-3 col-lg-4 col-md-6 col-sm-12">
                    <div className="single-product-item mb-60 wow fadeInUp">
                      <div className="product-img">
                        <img src="assets/images/products/img-1.png" alt="" />
                        <div className="pc-btn">Food</div>
                        <div className="cart-button">
                          <Link href="/products">
                            <a className="main-btn btn-yellow">Add to cart</a>
                          </Link>
                        </div>
                      </div>
                      <div className="product-info">
                        <ul className="ratings">
                          <li>
                            <i className="fas fa-star" />
                          </li>
                          <li>
                            <i className="fas fa-star" />
                          </li>
                          <li>
                            <i className="fas fa-star" />
                          </li>
                          <li>
                            <i className="fas fa-star" />
                          </li>
                          <li>
                            <i className="fas fa-star" />
                          </li>
                        </ul>
                        <h3 className="title">
                          <Link href="/product-details">
                            <a>Organice Delicious Pomegranate</a>
                          </Link>
                        </h3>
                        <span className="price">
                          <span className="curreny">$</span>53.56
                        </span>
                      </div>
                    </div>
                  </div>
                  <div className="col-xl-3 col-lg-4 col-md-6 col-sm-12">
                    <div className="single-product-item mb-60 wow fadeInDown">
                      <div className="product-img">
                        <img src="assets/images/products/img-2.png" alt="" />
                        <div className="pc-btn">Fish</div>
                        <div className="cart-button">
                          <Link href="/products">
                            <a className="main-btn btn-yellow">Add to cart</a>
                          </Link>
                        </div>
                      </div>
                      <div className="product-info">
                        <ul className="ratings">
                          <li>
                            <i className="fas fa-star" />
                          </li>
                          <li>
                            <i className="fas fa-star" />
                          </li>
                          <li>
                            <i className="fas fa-star" />
                          </li>
                          <li>
                            <i className="fas fa-star" />
                          </li>
                          <li>
                            <i className="fas fa-star" />
                          </li>
                        </ul>
                        <h3 className="title">
                          <Link href="/product-details">
                            <a>100% Natural Fresh Sea Fish</a>
                          </Link>
                        </h3>
                        <span className="price">
                          <span className="curreny">$</span>53.56
                        </span>
                      </div>
                    </div>
                  </div>
                  <div className="col-xl-3 col-lg-4 col-md-6 col-sm-12">
                    <div className="single-product-item mb-60 wow fadeInUp">
                      <div className="product-img">
                        <img src="assets/images/products/img-3.png" alt="" />
                        <div className="pc-btn">Food</div>
                        <div className="cart-button">
                          <Link href="/products">
                            <a className="main-btn btn-yellow">Add to cart</a>
                          </Link>
                        </div>
                      </div>
                      <div className="product-info">
                        <ul className="ratings">
                          <li>
                            <i className="fas fa-star" />
                          </li>
                          <li>
                            <i className="fas fa-star" />
                          </li>
                          <li>
                            <i className="fas fa-star" />
                          </li>
                          <li>
                            <i className="fas fa-star" />
                          </li>
                          <li>
                            <i className="fas fa-star" />
                          </li>
                        </ul>
                        <h3 className="title">
                          <Link href="/product-details">
                            <a>Organice Delicious Pomegranate</a>
                          </Link>
                        </h3>
                        <span className="price">
                          <span className="curreny">$</span>53.56
                        </span>
                      </div>
                    </div>
                  </div>
                  <div className="col-xl-3 col-lg-4 col-md-6 col-sm-12">
                    <div className="single-product-item mb-60 wow fadeInDown">
                      <div className="product-img">
                        <img src="assets/images/products/img-4.png" alt="" />
                        <div className="pc-btn">Vegetable</div>
                        <div className="cart-button">
                          <Link href="/products">
                            <a className="main-btn btn-yellow">Add to cart</a>
                          </Link>
                        </div>
                      </div>
                      <div className="product-info">
                        <ul className="ratings">
                          <li>
                            <i className="fas fa-star" />
                          </li>
                          <li>
                            <i className="fas fa-star" />
                          </li>
                          <li>
                            <i className="fas fa-star" />
                          </li>
                          <li>
                            <i className="fas fa-star" />
                          </li>
                          <li>
                            <i className="fas fa-star" />
                          </li>
                        </ul>
                        <h3 className="title">
                          <Link href="/product-details">
                            <a>Organice Delicious Pomegranate</a>
                          </Link>
                        </h3>
                        <span className="price">
                          <span className="curreny">$</span>53.56
                        </span>
                      </div>
                    </div>
                  </div>
                  <div className="col-xl-3 col-lg-4 col-md-6 col-sm-12">
                    <div className="single-product-item mb-60 wow fadeInUp">
                      <div className="product-img">
                        <img src="assets/images/products/img-5.png" alt="" />
                        <div className="pc-btn">Fruits</div>
                        <div className="cart-button">
                          <Link href="/products">
                            <a className="main-btn btn-yellow">Add to cart</a>
                          </Link>
                        </div>
                      </div>
                      <div className="product-info">
                        <ul className="ratings">
                          <li>
                            <i className="fas fa-star" />
                          </li>
                          <li>
                            <i className="fas fa-star" />
                          </li>
                          <li>
                            <i className="fas fa-star" />
                          </li>
                          <li>
                            <i className="fas fa-star" />
                          </li>
                          <li>
                            <i className="fas fa-star" />
                          </li>
                        </ul>
                        <h3 className="title">
                          <Link href="/product-details">
                            <a>Organice Delicious Pomegranate</a>
                          </Link>
                        </h3>
                        <span className="price">
                          <span className="curreny">$</span>53.56
                        </span>
                      </div>
                    </div>
                  </div>
                  <div className="col-xl-3 col-lg-4 col-md-6 col-sm-12">
                    <div className="single-product-item mb-60 wow fadeInDown">
                      <div className="product-img">
                        <img src="assets/images/products/img-6.png" alt="" />
                        <div className="pc-btn">Orange</div>
                        <div className="cart-button">
                          <Link href="/products">
                            <a className="main-btn btn-yellow">Add to cart</a>
                          </Link>
                        </div>
                      </div>
                      <div className="product-info">
                        <ul className="ratings">
                          <li>
                            <i className="fas fa-star" />
                          </li>
                          <li>
                            <i className="fas fa-star" />
                          </li>
                          <li>
                            <i className="fas fa-star" />
                          </li>
                          <li>
                            <i className="fas fa-star" />
                          </li>
                          <li>
                            <i className="fas fa-star" />
                          </li>
                        </ul>
                        <h3 className="title">
                          <Link href="/product-details">
                            <a>Organice Delicious Pomegranate</a>
                          </Link>
                        </h3>
                        <span className="price">
                          <span className="curreny">$</span>53.56
                        </span>
                      </div>
                    </div>
                  </div>
                  <div className="col-xl-3 col-lg-4 col-md-6 col-sm-12">
                    <div className="single-product-item mb-60 wow fadeInUp">
                      <div className="product-img">
                        <img src="assets/images/products/img-7.png" alt="" />
                        <div className="pc-btn">Fruits</div>
                        <div className="cart-button">
                          <Link href="/products">
                            <a className="main-btn btn-yellow">Add to cart</a>
                          </Link>
                        </div>
                      </div>
                      <div className="product-info">
                        <ul className="ratings">
                          <li>
                            <i className="fas fa-star" />
                          </li>
                          <li>
                            <i className="fas fa-star" />
                          </li>
                          <li>
                            <i className="fas fa-star" />
                          </li>
                          <li>
                            <i className="fas fa-star" />
                          </li>
                          <li>
                            <i className="fas fa-star" />
                          </li>
                        </ul>
                        <h3 className="title">
                          <Link href="/product-details">
                            <a>Organice Delicious Pomegranate</a>
                          </Link>
                        </h3>
                        <span className="price">
                          <span className="curreny">$</span>53.56
                        </span>
                      </div>
                    </div>
                  </div>
                  <div className="col-xl-3 col-lg-4 col-md-6 col-sm-12">
                    <div className="single-product-item mb-60 wow fadeInDown">
                      <div className="product-img">
                        <img src="assets/images/products/img-8.png" alt="" />
                        <div className="pc-btn">Fish</div>
                        <div className="cart-button">
                          <Link href="/products">
                            <a className="main-btn btn-yellow">Add to cart</a>
                          </Link>
                        </div>
                      </div>
                      <div className="product-info">
                        <ul className="ratings">
                          <li>
                            <i className="fas fa-star" />
                          </li>
                          <li>
                            <i className="fas fa-star" />
                          </li>
                          <li>
                            <i className="fas fa-star" />
                          </li>
                          <li>
                            <i className="fas fa-star" />
                          </li>
                          <li>
                            <i className="fas fa-star" />
                          </li>
                        </ul>
                        <h3 className="title">
                          <Link href="/product-details">
                            <a>Organice Delicious Pomegranate</a>
                          </Link>
                        </h3>
                        <span className="price">
                          <span className="curreny">$</span>53.56
                        </span>
                      </div>
                    </div>
                  </div>
                  <div className="col-xl-3 col-lg-4 col-md-6 col-sm-12">
                    <div className="single-product-item mb-60 wow fadeInUp">
                      <div className="product-img">
                        <img src="assets/images/products/img-9.png" alt="" />
                        <div className="pc-btn">Banana</div>
                        <div className="cart-button">
                          <Link href="/products">
                            <a className="main-btn btn-yellow">Add to cart</a>
                          </Link>
                        </div>
                      </div>
                      <div className="product-info">
                        <ul className="ratings">
                          <li>
                            <i className="fas fa-star" />
                          </li>
                          <li>
                            <i className="fas fa-star" />
                          </li>
                          <li>
                            <i className="fas fa-star" />
                          </li>
                          <li>
                            <i className="fas fa-star" />
                          </li>
                          <li>
                            <i className="fas fa-star" />
                          </li>
                        </ul>
                        <h3 className="title">
                          <Link href="/product-details">
                            <a>Organice Delicious Pomegranate</a>
                          </Link>
                        </h3>
                        <span className="price">
                          <span className="curreny">$</span>53.56
                        </span>
                      </div>
                    </div>
                  </div>
                  <div className="col-xl-3 col-lg-4 col-md-6 col-sm-12">
                    <div className="single-product-item mb-60 wow fadeInUp">
                      <div className="product-img">
                        <img src="assets/images/products/img-10.png" alt="" />
                        <div className="pc-btn">Banana</div>
                        <div className="cart-button">
                          <Link href="/products">
                            <a className="main-btn btn-yellow">Add to cart</a>
                          </Link>
                        </div>
                      </div>
                      <div className="product-info">
                        <ul className="ratings">
                          <li>
                            <i className="fas fa-star" />
                          </li>
                          <li>
                            <i className="fas fa-star" />
                          </li>
                          <li>
                            <i className="fas fa-star" />
                          </li>
                          <li>
                            <i className="fas fa-star" />
                          </li>
                          <li>
                            <i className="fas fa-star" />
                          </li>
                        </ul>
                        <h3 className="title">
                          <Link href="/product-details">
                            <a>Organice Delicious Pomegranate</a>
                          </Link>
                        </h3>
                        <span className="price">
                          <span className="curreny">$</span>53.56
                        </span>
                      </div>
                    </div>
                  </div>
                  <div className="col-xl-3 col-lg-4 col-md-6 col-sm-12">
                    <div className="single-product-item mb-60 wow fadeInUp">
                      <div className="product-img">
                        <img src="assets/images/products/img-11.png" alt="" />
                        <div className="pc-btn">Banana</div>
                        <div className="cart-button">
                          <Link href="/products">
                            <a className="main-btn btn-yellow">Add to cart</a>
                          </Link>
                        </div>
                      </div>
                      <div className="product-info">
                        <ul className="ratings">
                          <li>
                            <i className="fas fa-star" />
                          </li>
                          <li>
                            <i className="fas fa-star" />
                          </li>
                          <li>
                            <i className="fas fa-star" />
                          </li>
                          <li>
                            <i className="fas fa-star" />
                          </li>
                          <li>
                            <i className="fas fa-star" />
                          </li>
                        </ul>
                        <h3 className="title">
                          <Link href="/product-details">
                            <a>Organice Delicious Pomegranate</a>
                          </Link>
                        </h3>
                        <span className="price">
                          <span className="curreny">$</span>53.56
                        </span>
                      </div>
                    </div>
                  </div>
                  <div className="col-xl-3 col-lg-4 col-md-6 col-sm-12">
                    <div className="single-product-item mb-60 wow fadeInUp">
                      <div className="product-img">
                        <img src="assets/images/products/img-12.png" alt="" />
                        <div className="pc-btn">Banana</div>
                        <div className="cart-button">
                          <Link href="/products">
                            <a className="main-btn btn-yellow">Add to cart</a>
                          </Link>
                        </div>
                      </div>
                      <div className="product-info">
                        <ul className="ratings">
                          <li>
                            <i className="fas fa-star" />
                          </li>
                          <li>
                            <i className="fas fa-star" />
                          </li>
                          <li>
                            <i className="fas fa-star" />
                          </li>
                          <li>
                            <i className="fas fa-star" />
                          </li>
                          <li>
                            <i className="fas fa-star" />
                          </li>
                        </ul>
                        <h3 className="title">
                          <Link href="/product-details">
                            <a>Organice Delicious Pomegranate</a>
                          </Link>
                        </h3>
                        <span className="price">
                          <span className="curreny">$</span>53.56
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section> */}
    </Layout>
  );
};
export default Shop;
