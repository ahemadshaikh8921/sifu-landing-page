import PageBanner from "../src/components/PageBanner";
// import PortfolioGridIsotope from "../src/components/PortfolioGridIsotope";
import Link from "next/link";
import Layout from "../src/layouts/Layout";

import dynamic from "next/dynamic";

const PortfolioGridIsotope = dynamic(
  () => import("../src/components/PortfolioGridIsotope"),
  {
    ssr: false,
  }
);

const Portfolio = () => {
  return (
    <Layout>
      <PageBanner pageTitle={"Portfolio"} pageName="Portfolio Grid" />
      <section
        className="project-grid-page p-r z-1 pt-170 pb-130"
        id="project-filter"
      >
        <div className="container">
          <div className="row justify-content-center">
            <div className="col-xl-7 col-lg-10">
              <div className="section-title text-center mb-50 wow fadeInUp">
                <span className="sub-title">Project Gallery</span>
                <h2>
                  We’ve Done Many Other Projects Let’s See Gallery Insights
                </h2>
              </div>
            </div>
          </div>
          <PortfolioGridIsotope Farms={Farms} Nursery={Nursery} People={People} />
          <div className="row">
            <div className="col-lg-12">
              <div className="button-box text-center mt-30 wow fadeInDown">
                <Link href="/portfoloio-grid">
                  <a className="main-btn bordered-btn bordered-yellow">
                    View More Projects
                  </a>
                </Link>
              </div>
            </div>
          </div>
        </div>
      </section>
    </Layout>
  );
};
export default Portfolio;

const Nursery = [
  {
    id: 1,
    selector: 'cat-2',
    photo: 'assets/images/portfolio/nursery-1.JPG',
    linkTo: '/portfolio-details',
    linkText: 'Organic Grap',
    p1text: 'Agriculture',
    p2Text: 'Foods'
  },
  {
    id: 2,
    selector: 'cat-2',
    photo: 'assets/images/portfolio/nursery-2.JPG',
    linkTo: '/portfolio-details',
    linkText: 'Organic Grap',
    p1text: 'Agriculture',
    p2Text: 'Foods'
  },
  {
    id: 3,
    selector: 'cat-2',
    photo: 'assets/images/portfolio/nursery-3.JPG',
    linkTo: '/portfolio-details',
    linkText: 'Organic Grap',
    p1text: 'Agriculture',
    p2Text: 'Foods'
  },
  {
    id: 4,
    selector: 'cat-2',
    photo: 'assets/images/portfolio/nursery-4.JPG',
    linkTo: '/portfolio-details',
    linkText: 'Organic Grap',
    p1text: 'Agriculture',
    p2Text: 'Foods'
  },
  {
    id: 5,
    selector: 'cat-2',
    photo: 'assets/images/portfolio/nursery-5.JPG',
    linkTo: '/portfolio-details',
    linkText: 'Organic Grap',
    p1text: 'Agriculture',
    p2Text: 'Foods'
  },
  {
    id: 6,
    selector: 'cat-2',
    photo: 'assets/images/portfolio/nursery-6.JPG',
    linkTo: '/portfolio-details',
    linkText: 'Organic Grap',
    p1text: 'Agriculture',
    p2Text: 'Foods'
  },
  {
    id: 7,
    selector: 'cat-2',
    photo: 'assets/images/portfolio/nursery-7.JPG',
    linkTo: '/portfolio-details',
    linkText: 'Organic Grap',
    p1text: 'Agriculture',
    p2Text: 'Foods'
  },
  {
    id: 8,
    selector: 'cat-2',
    photo: 'assets/images/portfolio/nursery-8.JPG',
    linkTo: '/portfolio-details',
    linkText: 'Organic Grap',
    p1text: 'Agriculture',
    p2Text: 'Foods'
  },
  {
    id: 9,
    selector: 'cat-2',
    photo: 'assets/images/portfolio/nursery-9.png',
    linkTo: '/portfolio-details',
    linkText: 'Organic Grap',
    p1text: 'Agriculture',
    p2Text: 'Foods'
  }
]

// all portfolio data
const Farms = [
  {
    id: 1,
    selector: 'cat-1',
    photo: 'assets/images/portfolio/farm-1.png',
    linkTo: '/portfolio-details',
    linkText: 'Golder Wheat',
    p1text: 'Agriculture',
    p2Text: 'Foods'
  },
  {
    id: 2,
    selector: 'cat-1',
    photo: 'assets/images/portfolio/farm-2.png',
    linkTo: '/portfolio-details',
    linkText: 'Golder Wheat',
    p1text: 'Agriculture',
    p2Text: 'Foods'
  },
  {
    id: 3,
    selector: 'cat-1',
    photo: 'assets/images/portfolio/farm-3.png',
    linkTo: '/portfolio-details',
    linkText: 'Golder Wheat',
    p1text: 'Agriculture',
    p2Text: 'Foods'
  },
  {
    id: 4,
    selector: 'cat-1',
    photo: 'assets/images/portfolio/farm-4.png',
    linkTo: '/portfolio-details',
    linkText: 'Golder Wheat',
    p1text: 'Agriculture',
    p2Text: 'Foods'
  },
  {
    id: 5,
    selector: 'cat-1',
    photo: 'assets/images/portfolio/farm-5.png',
    linkTo: '/portfolio-details',
    linkText: 'Golder Wheat',
    p1text: 'Agriculture',
    p2Text: 'Foods'
  },
  {
    id: 6,
    selector: 'cat-1',
    photo: 'assets/images/portfolio/farm-6.png',
    linkTo: '/portfolio-details',
    linkText: 'Golder Wheat',
    p1text: 'Agriculture',
    p2Text: 'Foods'
  },
  {
    id: 7,
    selector: 'cat-1',
    photo: 'assets/images/portfolio/farm-7.png',
    linkTo: '/portfolio-details',
    linkText: 'Golder Wheat',
    p1text: 'Agriculture',
    p2Text: 'Foods'
  },
  {
    id: 8,
    selector: 'cat-1',
    photo: 'assets/images/portfolio/farm-8.png',
    linkTo: '/portfolio-details',
    linkText: 'Golder Wheat',
    p1text: 'Agriculture',
    p2Text: 'Foods'
  },
  {
    id: 9,
    selector: 'cat-1',
    photo: 'assets/images/portfolio/farm-9.png',
    linkTo: '/portfolio-details',
    linkText: 'Golder Wheat',
    p1text: 'Agriculture',
    p2Text: 'Foods'
  },
  {
    id: 10,
    selector: 'cat-1',
    photo: 'assets/images/portfolio/farm-10.png',
    linkTo: '/portfolio-details',
    linkText: 'Golder Wheat',
    p1text: 'Agriculture',
    p2Text: 'Foods'
  },
  {
    id: 11,
    selector: 'cat-1',
    photo: 'assets/images/portfolio/farm-11.png',
    linkTo: '/portfolio-details',
    linkText: 'Golder Wheat',
    p1text: 'Agriculture',
    p2Text: 'Foods'
  },
  {
    id: 12,
    selector: 'cat-1',
    photo: 'assets/images/portfolio/farm-12.png',
    linkTo: '/portfolio-details',
    linkText: 'Golder Wheat',
    p1text: 'Agriculture',
    p2Text: 'Foods'
  },

  {
    id: 13,
    selector: 'cat-1',
    photo: 'assets/images/portfolio/farm-13.png',
    linkTo: '/portfolio-details',
    linkText: 'Golder Wheat',
    p1text: 'Agriculture',
    p2Text: 'Foods'
  },
  {
    id: 14,
    selector: 'cat-1',
    photo: 'assets/images/portfolio/farm-14.png',
    linkTo: '/portfolio-details',
    linkText: 'Golder Wheat',
    p1text: 'Agriculture',
    p2Text: 'Foods'
  },
  {
    id: 15,
    selector: 'cat-1',
    photo: 'assets/images/portfolio/farm-15.png',
    linkTo: '/portfolio-details',
    linkText: 'Golder Wheat',
    p1text: 'Agriculture',
    p2Text: 'Foods'
  },
  {
    id: 16,
    selector: 'cat-1',
    photo: 'assets/images/portfolio/farm-16.png',
    linkTo: '/portfolio-details',
    linkText: 'Golder Wheat',
    p1text: 'Agriculture',
    p2Text: 'Foods'
  },
  {
    id: 17,
    selector: 'cat-1',
    photo: 'assets/images/portfolio/farm-17.png',
    linkTo: '/portfolio-details',
    linkText: 'Golder Wheat',
    p1text: 'Agriculture',
    p2Text: 'Foods'
  },
  {
    id: 18,
    selector: 'cat-1',
    photo: 'assets/images/portfolio/farm-18.png',
    linkTo: '/portfolio-details',
    linkText: 'Golder Wheat',
    p1text: 'Agriculture',
    p2Text: 'Foods'
  },
  {
    id: 19,
    selector: 'cat-1',
    photo: 'assets/images/portfolio/farm-19.png',
    linkTo: '/portfolio-details',
    linkText: 'Golder Wheat',
    p1text: 'Agriculture',
    p2Text: 'Foods'
  },
  {
    id: 20,
    selector: 'cat-1',
    photo: 'assets/images/portfolio/farm-20.png',
    linkTo: '/portfolio-details',
    linkText: 'Golder Wheat',
    p1text: 'Agriculture',
    p2Text: 'Foods'
  },
  {
    id: 21,
    selector: 'cat-1',
    photo: 'assets/images/portfolio/farm-21.png',
    linkTo: '/portfolio-details',
    linkText: 'Golder Wheat',
    p1text: 'Agriculture',
    p2Text: 'Foods'
  },

]

const People = [
  {
    id: 1,
    selector: 'cat-3',
    photo: 'assets/images/people/1.png',
  },
  {
    id: 2,
    selector: 'cat-3',
    photo: 'assets/images/people/13.png',

  },
  {
    id: 3,
    selector: 'cat-3',
    photo: 'assets/images/people/2.png',
  },
  {
    id: 4,
    selector: 'cat-3',
    photo: 'assets/images/people/3.png',
  },
  {
    selector: 'cat-3',
    id: 5,
    photo: 'assets/images/people/4.png',
  },
  {
    id: 6,
    selector: 'cat-3',
    photo: 'assets/images/people/5.png',
  },
  {
    id: 7,
    selector: 'cat-3',
    photo: 'assets/images/people/6.png',
  },
  {
    id: 8,
    selector: 'cat-3',
    photo: 'assets/images/people/7.png',
  },
  {
    selector: 'cat-3',
    id: 9,
    photo: 'assets/images/people/14.png',
  },
]