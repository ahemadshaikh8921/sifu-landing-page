import Link from "next/link";
import OrgariumCounter from "../src/components/OrgariumCounter";
import PageBanner from "../src/components/PageBanner";
// import PortFolioGallary from "../src/components/PortFolioGallary";

import dynamic from "next/dynamic";


const PortfolioGridIsotope = dynamic(
  () => import("../src/components/PortfolioGridIsotope"),
  {
    ssr: false,
  }
);


// facing issue on this components

import Layout from "../src/layouts/Layout";
import MeetTheTeam from "../src/components/MeetTheTeam";
import OurPeople from "../src/components/OurPeople";
const About = () => {
  return (
    <Layout>
      {/* Hero section start */}
      <PageBanner pageName={"About Us"} />
      {/* Hero section ends */}

      {/* Our Origin start */}
      <section className="about-section p-r z-1 pt-170 pb-80">
        <div className="container">
          <div className="row">
            <div className="col-lg-6">
              <div className="about-content-box content-box-gap mb-50">
                <div className="section-title section-title-left wow fadeInUp mb-30">
                  <span className="sub-title">Our Origin</span>
                  {/* <span className="sub-title">About Us</span> */}
                  <h2>{"We're the best agriculture & organic firm"}</h2>
                </div>
                <p style={{ textAlign: 'justify' }}>
                  {"From the success of Organic Lemon Myrtle Plantation Sdn Bhd, its founders founded Sifu Tani Sdn. Bhd. on October 2011 with the vision to be the catalyst in breeding lemon myrtle plantations and to cater to the growing demand of the global market for this nature’s miracle. The capital raised will support the development of the 120 acres of land in Sungai Tengi, Hulu Selangor, Selangor with the planting of Lemon Myrtle, the processing factory and mill (pharmaceutical grade), harvesting equipment, and workforce support to ensure the vision of being the leading producer, operator, and processor."}
                </p>
                <p style={{ textAlign: 'justify' }}>
                  <span style={{ fontStyle: 'italic' }}>Backhousia citriodora</span> (common names lemon myrtle, lemon scented myrtle, lemon scented ironwood) is a flowering plant in the family Myrtaceae, genus Backhousia. It is endemic to subtropical rainforests of central and south-eastern Queensland, Australia, with a natural distribution from Mackay to Brisbane. Other common names are sweet verbena tree, sweet verbena myrtle, (lemon-scented verbena is another species), and lemon-scented backhousia.
                </p>
                <p style={{ textAlign: 'justify' }}>
                  {'The common name reflects the strong lemon smell of crushed leaves. "Lemon scented myrtle" was the primary common name until the shortened trade name, "lemon myrtle", was created by the native foods industry to market the leaf for culinary use. Lemon myrtle is now the more common name for the plant and its products.'}
                </p>
                <div className="choose-item-list wow fadeInDown">
                  <div className="single-choose-item mb-30">
                    <div className="text">
                      <h5>100% Organic Product</h5>
                      <p>
                        Certified By Australian Certified Ltd.
                      </p>
                    </div>
                  </div>
                  {/* <div className="single-choose-item mb-30">
                    <div className="text">
                      <h5>Modern Euipments</h5>
                      <p>
                        Accusantium doloremque laudatium, totam rem aperiam
                        inventore veritatis et quasi architecto beatae{" "}
                      </p>
                    </div>
                  </div> */}
                  <div className="single-choose-item mb-30">
                    <div className="text">
                      <h5>Cost Advantage</h5>
                      <p>
                        The cost of plantation business in Malaysia is lower than in Australia, where Lemon Myrtle plantations primarily exist.
                      </p>
                    </div>
                  </div>
                  <div className="single-choose-item mb-30">
                    <div className="text">
                      <h5>Strategic Location</h5>
                      <p>
                        {"Malaysia's strategic location and logistics hubs would be an advantage to exporting Lemon Myrtle to the rest of the world."}
                      </p>
                    </div>
                  </div>
                  <div className="single-choose-item mb-30">
                    <div className="text">
                      <h5>Soil and Climate</h5>
                      <p>
                        Malaysian soil and weather are suitable for the clone used by Sifu Tani for its Lemon Myrtle Plantation.
                      </p>
                    </div>
                  </div>
                  <div className="single-choose-item mb-30">
                    <div className="text">
                      <h5>Technology and Sustainability</h5>
                      <p>
                        Sifu Tani utilizes new technology and sustainability for higher yield and better efficiency.
                      </p>
                    </div>
                  </div>
                </div>
                <div className="about-button wow fadeInUp">
                  <Link href="/about">
                    <a className="main-btn btn-yellow">Learn More Us</a>
                  </Link>
                </div>
              </div>
            </div>
            <div className="col-lg-6">
              <div className="about-four_image-box text-right p-r mb-50 wow fadeInRight">
                <img
                  src="assets/images/about/lemon.png"
                  className="about-img_one"
                  alt=""
                />
                <img
                  src="assets/images/about/i.png"
                  className="about-img_two"
                  alt=""
                />
              </div>
            </div>
          </div>
        </div>
      </section>
      {/* Our Origin ends */}

      {/*====== Meet The Team Section ======*/}
      <MeetTheTeam />
      {/*====== Meet The Team Section ======*/}

      {/*====== Our People section ======*/}
      {/* <OurPeople /> */}
      {/*====== Our People section ======*/}

      {/*====== Start Why Choose Section ======*/}
      <section className="why-choose-one p-r z-1 pt-130">
        <div className="shape shape-one">
          <span>
            <img src="assets/images/shape/leaf-3.png" alt="" />
          </span>
        </div>
        <div className="shape shape-two">
          <span>
            <img src="assets/images/shape/leaf-2.png" alt="" />
          </span>
        </div>
        <div className="container">
          <div className="row">
            <div className="col-lg-6">
              <div className="choose-one_img-box p-r mb-40 wow fadeInLeft">
                <img
                  src="assets/images/choose/img-5.png"
                  className="choose-img_one"
                  alt=""
                />
                <img
                  src="assets/images/choose/img-2.png"
                  className="choose-img_two"
                  alt=""
                />
              </div>
            </div>
            <div className="col-lg-6">
              <div className="choose-one_content-box pl-lg-70 mb-40">
                <div className="section-title section-title-left mb-40 wow fadeInDown">
                  <span className="sub-title">Value Propositions</span>
                  {/* <h2>Value Propositions</h2> */}
                </div>
                <div className="choose-item-list wow fadeInUp">
                  <div className="single-choose-item mb-30">
                    <div className="text">
                      <h5>Innovation & Continuous R&D</h5>
                      <p>
                        Sifu Tani consistently explores new products from Lemon Myrtle. Sifu Tani intends to be the first in the world to produce the Calcium Citrate byproduct in Lemon Myrtle which will be used for pharmaceuticals.
                      </p>
                    </div>
                  </div>
                  <div className="single-choose-item mb-30">
                    <div className="text">
                      <h5>Intellectual Property</h5>
                      <p>
                        Sifu Tani Sdn Bhd has direct access to the Intellectual Property, including plant material and products of Lemon myrtle.
                      </p>
                    </div>
                  </div>
                  <div className="single-choose-item mb-30">
                    <div className="text">
                      <h5>International Accreditation</h5>
                      <p>
                        Sifu Tani is internationally certified by USDA-NOP, EU Organic and ACO. Sifu Tani is also in the process of GMP, Halal and Kosher certification.
                      </p>
                    </div>
                  </div>
                  <div className="single-choose-item mb-30">
                    <div className="text">
                      <h5>Experienced & Credible Team</h5>
                      <p>
                        {"Sifu Tani's founders and management team has experience in the Lemon Myrtle industry of over 30 years."}
                      </p>
                    </div>
                  </div>
                </div>
                <div className="choose-button wow fadeInUp">
                  <Link href="/about">
                    <a className="main-btn bordered-btn bordered-yellow">
                      Learn About Us
                    </a>
                  </Link>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      {/*====== End Why Choose Section ======*/}

      {/*====== Certification Section ======*/}
      <section className="fun-fact-one pt-50 pb-50">
        <div className="section-title text-center mb-50 wow fadeInUp">
          <span className="sub-title">ACO CERTIFICATE LTD.</span>
          {/* <span className="sub-title">Certificate Of Compliance</span> */}
          <h2>
            {"Certificate Of Compliance"}
          </h2>
        </div>
        <div className="about-button text-center wow fadeInUp">
          <Link href="/certificate">
            <a target="_blank" className="main-btn btn-yellow">View Certificate</a>
          </Link>
        </div>
        {/* <div className="wow fadeInUp">
          <h2>Certification of Compliance</h2>

        </div> */}
        {/* <div className="container">
          <div className="counter-wrap-one wow fadeInDown">
            <div className="counter-inner-box">
              <OrgariumCounter />
            </div>
          </div>
        </div> */}
      </section>
      {/* Certification section ends */}

      {/* Our farm | Our nursery  */}
      <section
        className="project-grid-page p-r z-1 pt-100 pb-130"
        id="project-filter"
      >
        <div className="container">
          <div className="row justify-content-center">
            <div className="col-xl-7 col-lg-10">
              <div className="section-title text-center mb-50 wow fadeInUp">
                <span className="sub-title">Project Gallery</span>
                {/* <h2>
                  {"We’ve Done Many Other Projects Let’s See Gallery Insights"}
                </h2> */}
              </div>
            </div>
          </div>
          <PortfolioGridIsotope Farms={Farms} People={People} Nursery={Nursery} />
          <div className="row">
            <div className="col-lg-12">
              <div className="button-box text-center mt-30 wow fadeInDown">
                <Link href="/portfolio-grid">
                  <a className="main-btn bordered-btn bordered-yellow">
                    View More Projects
                  </a>
                </Link>
              </div>
            </div>
          </div>
        </div>
      </section>
      {/* Our farm | Our nursery  ends */}


    </Layout>
  );
};
export default About;

const People = [
  {
    id: 1,
    selector: 'cat-3',
    photo: 'assets/images/people/1.png',
  },
  {
    id: 2,
    selector: 'cat-3',
    photo: 'assets/images/people/13.png',

  },
  {
    id: 3,
    selector: 'cat-3',
    photo: 'assets/images/people/2.png',
  },
  {
    id: 4,
    selector: 'cat-3',
    photo: 'assets/images/people/3.png',
  },
  {
    selector: 'cat-3',
    id: 5,
    photo: 'assets/images/people/4.png',
  },
  {
    id: 6,
    selector: 'cat-3',
    photo: 'assets/images/people/5.png',
  },
]


const Farms = [
  {
    id: 1,
    selector: 'cat-1',
    photo: 'assets/images/portfolio/farm-1.png',
    linkTo: '/portfolio-details',
    linkText: 'Golder Wheat',
    p1text: 'Agriculture',
    p2Text: 'Foods'
  },
  {
    id: 2,
    selector: 'cat-1',
    photo: 'assets/images/portfolio/farm-2.png',
    linkTo: '/portfolio-details',
    linkText: 'Golder Wheat',
    p1text: 'Agriculture',
    p2Text: 'Foods'
  },
  {
    id: 3,
    selector: 'cat-1',
    photo: 'assets/images/portfolio/farm-3.png',
    linkTo: '/portfolio-details',
    linkText: 'Golder Wheat',
    p1text: 'Agriculture',
    p2Text: 'Foods'
  },
  {
    id: 4,
    selector: 'cat-1',
    photo: 'assets/images/portfolio/farm-4.png',
    linkTo: '/portfolio-details',
    linkText: 'Golder Wheat',
    p1text: 'Agriculture',
    p2Text: 'Foods'
  },
  {
    id: 5,
    selector: 'cat-1',
    photo: 'assets/images/portfolio/farm-5.png',
    linkTo: '/portfolio-details',
    linkText: 'Golder Wheat',
    p1text: 'Agriculture',
    p2Text: 'Foods'
  },
  {
    id: 6,
    selector: 'cat-1',
    photo: 'assets/images/portfolio/farm-6.png',
    linkTo: '/portfolio-details',
    linkText: 'Golder Wheat',
    p1text: 'Agriculture',
    p2Text: 'Foods'
  },

]

const Nursery = [
  {
    id: 1,
    selector: 'cat-2',
    photo: 'assets/images/portfolio/nursery-1.JPG',
    linkTo: '/portfolio-details',
    linkText: 'Organic Grap',
    p1text: 'Agriculture',
    p2Text: 'Foods'
  },
  {
    id: 2,
    selector: 'cat-2',
    photo: 'assets/images/portfolio/nursery-2.JPG',
    linkTo: '/portfolio-details',
    linkText: 'Organic Grap',
    p1text: 'Agriculture',
    p2Text: 'Foods'
  },
  {
    id: 3,
    selector: 'cat-2',
    photo: 'assets/images/portfolio/nursery-3.JPG',
    linkTo: '/portfolio-details',
    linkText: 'Organic Grap',
    p1text: 'Agriculture',
    p2Text: 'Foods'
  },
  {
    id: 4,
    selector: 'cat-2',
    photo: 'assets/images/portfolio/nursery-4.JPG',
    linkTo: '/portfolio-details',
    linkText: 'Organic Grap',
    p1text: 'Agriculture',
    p2Text: 'Foods'
  },
  {
    id: 5,
    selector: 'cat-2',
    photo: 'assets/images/portfolio/nursery-5.JPG',
    linkTo: '/portfolio-details',
    linkText: 'Organic Grap',
    p1text: 'Agriculture',
    p2Text: 'Foods'
  },
  {
    id: 6,
    selector: 'cat-2',
    photo: 'assets/images/portfolio/nursery-6.JPG',
    linkTo: '/portfolio-details',
    linkText: 'Organic Grap',
    p1text: 'Agriculture',
    p2Text: 'Foods'
  },
]