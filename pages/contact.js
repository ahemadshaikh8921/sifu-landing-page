import Slider from "react-slick";
import PageBanner from "../src/components/PageBanner";
import Layout from "../src/layouts/Layout";
import { logoSlider } from "../src/sliderProps";
import Image from "next/image";
import { useState } from "react";

const Contact = () => {
  const [show, setShow] = useState(false)

  function encode(data) {
    return Object.keys(data)
      .map(key => encodeURIComponent(key) + "=" + encodeURIComponent(data[key]))
      .join("&")
  }

  const handleSubmit = (event) => {
    console.log("event", event)
    event.preventDefault()
    fetch("/", {
      method: "POST",
      headers: { "Content-Type": "application/x-www-form-urlencoded" },
      body: encode({
        "form-name": "contactForm",
        "name": event.target.elements.name.value,
        "email": event.target.elements.email.value,
        "message": event.target.elements.message.value,
      })
    }).then(() => { setShow(true); document.getElementById("myForm").reset() }).catch(error => alert(error))
  }



  return (
    <Layout>
      <PageBanner pageName={"Contact Us"} />
      <section className="contact-information-one p-r z-1 pt-215 pb-130">
        <div className="information-img_one wow fadeInRight">
          {/* <img src="assets/images/contact/img-1.jpg" alt="Imaged" /> */}
          <Image width={525} height={665} src="/assets/images/contact/img-3.png" alt="Imaged" />

        </div>
        <div className="container">
          <div className="row">
            <div className="col-xl-10 col-lg-12">
              <div className="contact-two_information-box">
                <div className="section-title section-title-left mb-50 wow fadeInUp">
                  <span className="sub-title">Get In Touch</span>
                  <h2>
                    We’re Ready To Help <br /> You!
                  </h2>
                </div>
                <div className="row">
                  <div className="col-lg-4 col-md-6 col-sm-12">
                    <div className="information-item-two info-one mb-30 wow fadeInDown">
                      <div className="icon">
                        <i className="far fa-map-marker-alt" />
                      </div>
                      <div className="info">
                        <h5>Locations</h5>
                        <p><span style={{ color: '#443f3f' }}>Sifu Tani HQ Office:-</span>  Petaling Jaya, Selangor.</p>
                        <p><span style={{ color: '#443f3f' }}>Sifu Tani Lemon Myrtle Orchard (SiLMO):-</span>  Kuala Kubu Baru, Selangor.</p>
                      </div>
                    </div>
                  </div>
                  <div className="col-lg-4 col-md-6 col-sm-12">
                    <div className="information-item-two mb-30 info-two wow fadeInUp">
                      <div className="icon">
                        <i className="far fa-envelope-open-text" />
                      </div>
                      <div className="info">
                        <h5>Email Address</h5>
                        <p>
                          <a href="mailto:silmo@sifutani.com">
                            silmo@sifutani.com
                          </a>
                        </p>
                        <p>
                          &nbsp;
                        </p>
                      </div>
                    </div>
                  </div>
                  <div className="col-lg-4 col-md-6 col-sm-12">
                    <div className="information-item-two mb-30 info-three wow fadeInDown">
                      <div className="icon">
                        <i className="far fa-phone" />
                      </div>
                      <div className="info">
                        <h5>Phone Number</h5>
                        <p>
                          <a href="tel:+60378598084">+603-78598084</a>
                        </p>
                        <p>
                          &nbsp;
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
                {/* <div className="row">
                  <div className="col-lg-8">
                    <p>
                      Natus error sit voluptatem accusantium doloremque
                      laudatium, totam rem aperiam eaque ipsa quae abllo
                      inventore veritatis et quase
                    </p>
                  </div>
                </div> */}
              </div>
            </div>
          </div>
        </div>
      </section>
      {/*====== End Contact Information section ======*/}
      {/*====== Start Map section ======*/}
      <section className="contact-page-map">
        <div className="map-box">
          <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3983.925921364395!2d101.57229041373563!3d3.1143011542368595!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31cc4f58cfa401e7%3A0x336659fec146eaf2!2sSIFU%20TANI%20SDN%20BHD%20(SiLMO)%20HQ!5e0!3m2!1sen!2sin!4v1669921422717!5m2!1sen!2sin" />
          {/* <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3983.925921364395!2d101.57229041373563!3d3.1143011542368595!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31cc4f58cfa401e7%3A0x336659fec146eaf2!2sSIFU%20TANI%20SDN%20BHD%20(SiLMO)%20HQ!5e0!3m2!1sen!2sin!4v1669921422717!5m2!1sen!2sin" width="800" height="600" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe> */}
        </div>
      </section>
      {/*====== End Map section ======*/}
      {/*====== Start Contact Section ======*/}
      <section className="contact-three pb-70 wow fadeInUp">
        <div className="container">
          <div className="row justify-content-end">
            <div className="col-xl-7 col-lg-10">
              <div className="contact-three_content-box">
                <div className="section-title section-title-left mb-60">
                  <span className="sub-title">Get In Touch</span>
                  <h2>Need Oragnic Products ? Send Us A Message Now !</h2>
                </div>
                <div className="contact-form">
                  <form id="myForm" netlify="true" name="contactForm" method="POST" data-netlify="true" onSubmit={handleSubmit}>
                  <input type="hidden" name="form-name" value="contactForm" />
                    <div className="form_group">
                      <input
                        type="text"
                        className="form_control"
                        placeholder="Full Name"
                        name="name"
                        required
                      />
                    </div>
                    <div className="form_group">
                      <input
                        type="email"
                        className="form_control"
                        placeholder="Email Address"
                        name="email"
                        required
                      />
                    </div>
                    <div className="form_group">
                      <textarea
                        className="form_control"
                        placeholder="Write Message"
                        name="message"

                      />
                    </div>
                    <button type="submit" className="main-btn btn-yellow">
                      Send Us Message
                    </button>
                    {/* </div> */}
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      {/*====== End Contact Section ======*/}
      {/*====== Start Partner Section ======*/}
      {/* <section className="partners-one p-r z-1 pt-50 pb-130">
        <div className="container">
          <div className="row justify-content-center">
            <div className="col-lg-6">
              <div className="section-title text-center mb-30 wow fadeInUp">
                <h4>We Have More Then 1235+ Global Partners</h4>
              </div>
            </div>
          </div>
          <Slider {...logoSlider} className="partner-slider-one wow fadeInDown">
            <div className="partner-item-two">
              <div className="partner-img">
                <img
                  src="assets/images/partner/img-7.png"
                  alt="partner image"
                />
              </div>
            </div>
            <div className="partner-item-two">
              <div className="partner-img">
                <img
                  src="assets/images/partner/img-8.png"
                  alt="partner image"
                />
              </div>
            </div>
            <div className="partner-item-two">
              <div className="partner-img">
                <img
                  src="assets/images/partner/img-9.png"
                  alt="partner image"
                />
              </div>
            </div>
            <div className="partner-item-two">
              <div className="partner-img">
                <img
                  src="assets/images/partner/img-10.png"
                  alt="partner image"
                />
              </div>
            </div>
            <div className="partner-item-two">
              <div className="partner-img">
                <img
                  src="assets/images/partner/img-11.png"
                  alt="partner image"
                />
              </div>
            </div>
            <div className="partner-item-two">
              <div className="partner-img">
                <img
                  src="assets/images/partner/img-12.png"
                  alt="partner image"
                />
              </div>
            </div>
            <div className="partner-item-two">
              <div className="partner-img">
                <img
                  src="assets/images/partner/img-10.png"
                  alt="partner image"
                />
              </div>
            </div>
          </Slider>
        </div>
      </section> */}
    </Layout>
  );
};
export default Contact;
