import Link from 'next/link'
import React from 'react'

const MeetTheTeam = () => {
    return (
        <>
            {/*====== Meet The Team Section ======*/}
            <section className="farmers-team_two light-gray-bg pb-90">
                <div className="container">
                    <div className="row align-items-end">
                        <div className="col-lg-8">
                            <div className="section-title section-title-left mb-50 wow fadeInLeft">
                                <span className="sub-title">Founders</span>
                                <h2>We have team members with decades of experience</h2>
                            </div>
                        </div>
                        {/* <div className="col-lg-4">
                            <div className="team-button float-lg-right mb-60 wow fadeInRight">
                                <Link href="/farmers">
                                    <a className="main-btn bordered-btn bordered-yellow">
                                        Become a Member
                                    </a>
                                </Link>
                            </div>
                        </div> */}
                    </div>
                    <div className="row justify-content-center">
                        <div className="col-xl-4 col-lg-4 col-md-6 col-sm-12">
                            <div className="team-member_one text-center mb-40 wow fadeInUp">
                                <div className="member-img">
                                    <img src="assets/images/team/bod-aziz.png" alt="" />
                                </div>
                                <div className="member-info">
                                    <h3 className="title">
                                        <Link href="#">
                                            <a>{"Dato' Aziz Bakar"}</a>
                                        </Link>
                                    </h3>
                                    <p className="position">Executive Chairman</p>
                                    <ul className="social-link">
                                        <li>
                                            <a href="#">
                                                <i className="fab fa-facebook-f" />
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <i className="fab fa-twitter" />
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <i className="fab fa-linkedin" />
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <i className="fab fa-youtube" />
                                            </a>
                                        </li>
                                    </ul>
                                    <p style={{ textAlign: 'justify', marginTop: '10px' }}>Holds a diploma in Agriculture Business from Kolej Partanian Malaysia, a BSC in Agriculture from Louisiana State University, USA, and an MBA from the University of Dallas, USA. He has over 15 years of experience with the founders of AirAsia and currently serves on the Board of another PLC and Astro foundation <br /> <br /> <br /></p>
                                </div>
                            </div>
                        </div>
                        <div className="col-xl-4 col-lg-4 col-md-6 col-sm-12">
                            <div className="team-member_one text-center mb-40 wow fadeInDown">
                                <div className="member-img">
                                    <img src="assets/images/team/azmi-lop.png" alt="" />
                                </div>
                                <div className="member-info">
                                    <h3 className="title">
                                        <Link href="#">
                                            <a>Md Azmi Lop Yusof</a>
                                        </Link>
                                    </h3>
                                    <p className="position">Chief Executive Officer</p>
                                    <ul className="social-link">
                                        <li>
                                            <a href="#">
                                                <i className="fab fa-facebook-f" />
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <i className="fab fa-twitter" />
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <i className="fab fa-linkedin" />
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <i className="fab fa-youtube" />
                                            </a>
                                        </li>
                                    </ul>
                                    <p style={{ textAlign: 'justify', marginTop: '10px' }}>Holds a Diploma in Agriculture from Agriculture Kolej pertanian Serdang in 1975, a BSc in Agriculture business from Louisiana State University, United States of America in 1978, and a Master in Agriculture Marketing from the West Texas University, United States of America in 1980. Md Azmi has Various experiences in Agriculture from operation to Business Development and Strategy</p>
                                </div>
                            </div>
                        </div>
                        <div className="col-xl-4 col-lg-4 col-md-6 col-sm-12">
                            <div className="team-member_one text-center mb-40 wow fadeInUp">
                                <div className="member-img">
                                    <img src="assets/images/team/ian-twyford.png" alt="" />
                                </div>
                                <div className="member-info">
                                    <h3 className="title">
                                        <Link href="#">
                                            <a>Ian Twyford</a>
                                        </Link>
                                    </h3>
                                    <p className="position">Technical Director</p>
                                    <ul className="social-link">
                                        <li>
                                            <a href="#">
                                                <i className="fab fa-facebook-f" />
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <i className="fab fa-twitter" />
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <i className="fab fa-linkedin" />
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <i className="fab fa-youtube" />
                                            </a>
                                        </li>
                                    </ul>
                                    <p style={{ textAlign: 'justify', marginTop: '10px' }}>Ian Twyford  is an experienced international Agricultural Manager with a background in project development and implementation in numerous countries such as Australia, Africa, and the West Indies. He holds a Diploma in Rural Techniques Horticulture from the University of Queensland, Gatton Lawes, Australia. He has over 30 years of experience in Agriculture, particularly organic tea.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            {/*====== Meet The Team Section ======*/}
        </>
    )
}

export default MeetTheTeam