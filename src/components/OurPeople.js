import Link from 'next/link'
import React from 'react'
// import Isotope from "isotope-layout";

import { Fragment, useEffect, useRef, useState } from "react";


const OurPeople = () => {

    // Isotope
    const isotope = useRef();
    const [filterKey, setFilterKey] = useState("*");
    useEffect(() => {
        // setTimeout(() => {
        //     isotope.current = new Isotope(".project-row", {
        //         itemSelector: ".project-column",
        //         //  layoutMode: "fitRows",
        //         percentPosition: true,
        //         masonry: {
        //             columnWidth: ".project-column",
        //         },
        //         animationOptions: {
        //             duration: 750,
        //             easing: "linear",
        //             queue: false,
        //         },
        //     });
        // }, 1000);
        //     return () => isotope.current.destroy();
    }, []);
    useEffect(() => {
        if (isotope.current) {
            filterKey === "*"
                ? isotope.current.arrange({ filter: `*` })
                : isotope.current.arrange({ filter: `.${filterKey}` });
        }
    }, [filterKey]);
    const handleFilterKeyChange = (key) => () => {
        setFilterKey(key);
    };
    const activeBtn = (value) => (value === filterKey ? "active" : "");



    return (
        <>
            <section
                className="project-grid-page p-r z-1 pt-100 pb-130"
                id="project-filter"
            >
                <div className="container">
                    <div className="row justify-content-center">
                        <div className="col-xl-7 col-lg-10">
                            <div className="section-title text-center mb-50 wow fadeInUp">
                                <span className="sub-title">Our People</span>
                                <h2>
                                    {"We Have Lot’s Of Experience People"}
                                </h2>
                            </div>
                        </div>
                    </div>

                    {/* starts */}
                    <Fragment>
                        {/* <div className="row justify-content-center">
                            <div className="col-lg-10">
                                <div className="portfolio-filter-button text-center mb-60 wow fadeInDown">
                                    <ul className="filter-btn">
                                        <li
                                            className={`c-pointer ${activeBtn("*")}`}
                                            onClick={handleFilterKeyChange("*")}
                                            data-filter="*"
                                        >
                                            Our Farm
                                        </li>
                                        <li
                                            className={`c-pointer ${activeBtn("cat-1")}`}
                                            onClick={handleFilterKeyChange("cat-1")}
                                            data-filter=".cat-1"
                                        >
                                            Our Farm

                                        </li>
                                        <li
                                            className={`c-pointer ${activeBtn("cat-2")}`}
                                            onClick={handleFilterKeyChange("cat-2")}
                                            data-filter=".cat-2"
                                        >
                                            Our nursery
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div> */}
                        <div className="row project-row">
                            {
                                Farms.map(x => {
                                    return (
                                        <div key={x.id} className="col-lg-4 col-md-6 col-sm-12 project-column ">
                                            <div className="project-item-three mb-30 wow fadeInUp">
                                                <div className="img-holder">
                                                    <img src={x.photo} alt="" />
                                                    {/* <div className="hover-portfolio">
                                                        <div className="icon-btn">
                                                            <Link href={x.linkTo}>
                                                                <a>
                                                                    <i className="far fa-arrow-right" />
                                                                </a>
                                                            </Link>
                                                        </div>
                                                        <div className="hover-content">
                                                            <h3 className="title">
                                                                <Link href={x.linkTo}>
                                                                    <a>{x.linkText}</a>
                                                                </Link>
                                                            </h3>
                                                            <p>
                                                                <a href="#">{x.p1text}</a>,<a href="#">{x.p2Text}</a>
                                                            </p>
                                                        </div>
                                                    </div> */}
                                                </div>
                                            </div>
                                        </div>
                                    )
                                })
                            }
                        </div>
                    </Fragment>

                    {/* <div className="row">
                        <div className="col-lg-12">
                            <div className="button-box text-center mt-30 wow fadeInDown">
                                <Link href="/portfolio-grid">
                                    <a className="main-btn bordered-btn bordered-yellow">
                                        View More Projects
                                    </a>
                                </Link>
                            </div>
                        </div>
                    </div> */}
                </div>
            </section>
        </>
    )
}

export default OurPeople



const Farms = [
    {
        id: 1,
        photo: 'assets/images/people/1.png',
    },
    {
        id: 2,
        photo: 'assets/images/people/13.png',
        
    },
    {
        id: 3,
        photo: 'assets/images/people/2.png',
    },
    {
        id: 4,
        photo: 'assets/images/people/3.png',
    },
    {
        id: 5,
        photo: 'assets/images/people/4.png',
    },
    {
        id: 6,
        photo: 'assets/images/people/5.png',
    },
    {
        id: 7,
        photo: 'assets/images/people/6.png',
    },
    {
        id: 8,
        photo: 'assets/images/people/7.png',
    },
    {
        id: 9,
        photo: 'assets/images/people/14.png',
    },

]
