import Isotope from "isotope-layout";
import Link from "next/link";

import { Fragment, useEffect, useRef, useState } from "react";
const PortfolioGridIsotope = ({ Farms, Nursery, People }) => {
  const [mainData, setMainData] = useState(Farms);

  // Isotope
  const isotope = useRef();
  const [filterKey, setFilterKey] = useState("cat-1");
  useEffect(() => {
    setTimeout(() => {
      isotope.current = new Isotope(".project-row", {
        itemSelector: ".project-column",
        //  layoutMode: "fitRows",
        percentPosition: true,
        masonry: {
          columnWidth: ".project-column",
        },
        animationOptions: {
          duration: 750,
          easing: "linear",
          queue: false,
        },
      });
    }, 1000);
    //     return () => isotope.current.destroy();
  }, [mainData]);
  useEffect(() => {
    if (isotope.current) {
      filterKey === "cat-1"
        ? isotope.current.arrange({ filter: `.cat-1` })
        : isotope.current.arrange({ filter: `.${filterKey}` });
    }
  }, [mainData]);
  const handleFilterKeyChange = (key) => () => {
    setFilterKey(key);
    if(key==='cat-1'){
      setMainData(Farms)
    }else if(key==='cat-2'){
      setMainData(Nursery) 
    }else if(key==='cat-3'){
      setMainData(People);
    }
  };
  const activeBtn = (value) => (value === filterKey ? "active" : "");
  return (
    <Fragment>
      <div className="row justify-content-center">
        <div className="col-lg-10">
          <div className="portfolio-filter-button text-center mb-60 wow fadeInDown">
            <ul className="filter-btn">
              {/* <li
                className={`c-pointer ${activeBtn("*")}`}
                onClick={handleFilterKeyChange("*")}
                data-filter="*"
              >
                Our Farm
              </li> */}
              <li
                className={`c-pointer ${activeBtn("cat-1")}`}
                onClick={handleFilterKeyChange("cat-1")}
                data-filter=".cat-1"
              >
                Our Farm

              </li>
              <li
                className={`c-pointer ${activeBtn("cat-2")}`}
                onClick={handleFilterKeyChange("cat-2")}
                data-filter=".cat-2"
              >
                Our Nursery
              </li>
              <li
                className={`c-pointer ${activeBtn("cat-3")}`}
                onClick={handleFilterKeyChange("cat-3")}
                data-filter=".cat-2"
              >
                Our People
              </li>
            </ul>
          </div>
        </div>
      </div>
      <div className="row project-row">
        {
          mainData.map(x => {
            return (
              <div key={x.id} className={"col-lg-4 col-md-6 col-sm-12 project-column " + x.selector}>
                <div className="project-item-three mb-30 wow fadeInUp">
                  <div className="img-holder">
                    <img src={x.photo} width={300} height={250} alt="" />
                    {/* <div className="hover-portfolio">
                      <div className="icon-btn">
                        <Link href={x.linkTo}>
                          <a>
                            <i className="far fa-arrow-right" />
                          </a>
                        </Link>
                      </div>
                      <div className="hover-content">
                        <h3 className="title">
                          <Link href={x.linkTo}>
                            <a>{x.linkText}</a>
                          </Link>
                        </h3>
                        <p>
                          <a href="#">{x.p1text}</a>,<a href="#">{x.p2Text}</a>
                        </p>
                      </div>
                    </div> */}
                  </div>
                </div>
              </div>
            )
          }) 
          
        }
      </div>
    </Fragment>
  );
};
export default PortfolioGridIsotope;


