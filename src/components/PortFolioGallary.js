import Link from 'next/link';
import React, { useEffect, useState } from 'react';

const PortFolioGallary = () => {
    const [data, setData] = useState(DATA.filter( x => x.selector === 'ourFarm'));
    const [ourfarmActive, setOurFarmActive] = useState('active');
    const [ourNurseryActive, setOurNurseryActive] = useState('');

    useEffect(()=>{}, [data]);

    const handleFilterKeyChange = (key) => {
        const filterdata = DATA.filter(x => key === x.selector);
        setData(filterdata);
        console.log(filterdata, "filterdata");

        if (key == "ourFarm") {
            setOurFarmActive('active');
            setOurNurseryActive("");
        }
        else if (key == "ourNursery") {
            setOurFarmActive('');
            setOurNurseryActive("active");
        }
    }

    return (
        <>

            {/* options to be selected */}
            <div className="row justify-content-center">
                <div className="col-lg-10">
                    <div className="portfolio-filter-button text-center mb-60 wow fadeInDown">
                        <ul className="filter-btn">
                            <li
                                className={`c-pointer ${ourfarmActive}`}
                                onClick={() => handleFilterKeyChange("ourFarm")}
                                data-filter=".ourFarm"
                            >
                                Our Farm
                            </li>
                            <li
                                className={`c-pointer ${ourNurseryActive}`}
                                onClick={() => handleFilterKeyChange("ourNursery")}
                                data-filter=".ourNursery"
                            >
                                Our nursery
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            {/* data shown of selected options */}
            <div className="row project-row">
                {
                    data.map(x => {
                        return (
                            <div key={x.id} className={"col-lg-4 col-md-6 col-sm-12 project-column " + x.selector}>
                                <div className="project-item-three mb-30 wow fadeInUp">
                                    <div className="img-holder">
                                        <img src={x.photo} alt="" />
                                        <div className="hover-portfolio">
                                            <div className="icon-btn">
                                                <Link href={x.linkTo}>
                                                    <a>
                                                        <i className="far fa-arrow-right" />
                                                    </a>
                                                </Link>
                                            </div>
                                            <div className="hover-content">
                                                <h3 className="title">
                                                    <Link href={x.linkTo}>
                                                        <a>{x.linkText}</a>
                                                    </Link>
                                                </h3>
                                                <p>
                                                    <a href="#">{x.p1Text}</a>,<a href="#">{x.p2Text}</a>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        )
                    })
                }
            </div>


        </>
    )
}

export default PortFolioGallary


const DATA = [
    {
        id: 1,
        selector: 'ourFarm',
        photo: 'assets/images/portfolio/img-4.jpg',
        linkTo: '/portfolio-details',
        linkText: 'Golder Wheat',
        p1Text: 'Agriculture',
        p2Text: 'Foods'
    },
    {
        id: 2,
        selector: 'ourNursery',
        photo: 'assets/images/portfolio/img-5.jpg',
        linkTo: '/portfolio-details',
        linkText: 'Organic Grap',
        p1Text: 'Agriculture',
        p2Text: 'Foods'
    },
    {
        id: 3,
        selector: 'ourFarm',
        photo: 'assets/images/portfolio/img-9.jpg',
        linkTo: '/portfolio-details',
        linkText: 'Golder Wheat',
        p1Text: 'Agriculture',
        p2Text: 'Foods'
    },
    {
        id: 4,
        selector: 'ourNursery',
        photo: 'assets/images/portfolio/img-11.jpg',
        linkTo: '/portfolio-details',
        linkText: 'Golder Wheat',
        p1Text: 'Agriculture',
        p2Text: 'Foods'
    },
    {
        id: 5,
        selector: 'ourFarm',
        photo: 'assets/images/portfolio/img-6.jpg',
        linkTo: '/portfolio-details',
        linkText: 'Golder Wheat',
        p1Text: 'Agriculture',
        p2Text: 'Foods'
    },
    {
        id: 6,
        selector: 'ourNursery',
        photo: 'assets/images/portfolio/img-10.jpg',
        linkTo: '/portfolio-details',
        linkText: 'Golder Wheat',
        p1Text: 'Agriculture',
        p2Text: 'Foods'
    },
]